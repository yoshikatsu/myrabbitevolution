﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyBehaviour : MonoBehaviour {

    private bool dragging = false;
    public float offset = 0.05f;

    private float idlePos = 0;
    public int idleSpeed = 1;
    public float idleScale = 0.005f;
    private float internalTime = 0;

    public GameObject nextBunny;

    bool DragAndDrop()
    {
        if (Input.GetMouseButtonDown(0) && ((Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).magnitude <= offset))
        {
            dragging = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
            return (true);
        }
        if (dragging == true)
        {
            transform.position = Vector2.Lerp(transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition), 1);
        }
        return (false);
    }

    // Use this for initialization
    void Start () {
        offset += 10;
    }
	
	// Update is called once per frame
	void Update () {
        internalTime += Time.deltaTime;
        idlePos = Mathf.Sin(internalTime * idleSpeed) * idleScale;
        transform.position = new Vector2(transform.position.x, transform.position.y + idlePos);
        DragAndDrop();
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (dragging == true && this.name == collision.name)
        {
            Instantiate(nextBunny, collision.gameObject.transform.position, transform.rotation);
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}
