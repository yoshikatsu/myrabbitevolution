﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBehaviour : MonoBehaviour {

    private Vector2 pos;

    public GameObject bunny;

    // Use this for initialization
    void Start () {
        pos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown ()
    {
        Instantiate(bunny, pos, transform.rotation);
        Destroy(this.gameObject);
    }
}
