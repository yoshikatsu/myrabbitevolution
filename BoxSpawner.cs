﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour {

    public int spawnRate = 5;
    private int spawnTime = 0;

    public int xMin;
    public int xMax;

    public int yMin;
    public int yMax;

    public GameObject box;

	// Use this for initialization
	void Start () {
    }
	
    void SpawnBox()
    {
        Vector2 pos = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));

        Instantiate(box, pos, transform.rotation);
    }

	// Update is called once per frame
	void Update () {
        if (Time.time > spawnTime)
        {
            spawnTime += spawnRate;
            SpawnBox();
        }
	}
}
